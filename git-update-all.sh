#!/bin/bash

# Update all directories, where this file is placed into

for dir in */
do
    echo ""
    if [[ -d "$dir/.git" ]]; then
        echo "Updating Directory   $dir"
        cd "$dir"
        git fetch
        git pull
        cd ..
    else
        echo "Skipping Directory   $dir"
    fi
done
