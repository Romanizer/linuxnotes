# generate and add an ssh key to remote server for password-less login

# first, generate a key (no passphrase and default path: .ssh/id_rsa)
# skip if there is already a key present at .ssh/id_rsa(.pub)
ssh-keygen -t rsa

## Method 1
# copy the public key to remote
ssh-copy-id remote

## Method 2
# on the remote server, if not present already
ssh user@remote 'mkdir -p .ssh'
# append the public ssh key to .ssh/authorized_keys on the remote
cat .ssh/id_rsa.pub | ssh user@remote 'cat >> .ssh/authorized_keys'

## Finally
# check if login without password works
ssh user@remote


# if it does not work, try these solutions:
## on remote: modify access right of .ssh
chmod 700 .ssh

## transmit the public key to .ssh/authorized_keys2 instead

## on remote: set permissions of authorized_keys(2) to 640
chmod 640 .ssh/authorized_keys*
