# generate a new ssh key to send to gitlab to auth:
ssh-keygen -t ed25519 -C "gitlab key"

# next open the public part of your key
# copy it and give it gitlab:
# Avatar -> edit profile -> SSH Keys

# to verify it worked:
ssh -T git@gitlab.com

# to clone repositories:
git clone git@gitlab.com:USERNAME/REPO.git
# or in an organization
git clone git@USERNAME.gitlab.com:GITLAB-ORG/REPO.git
