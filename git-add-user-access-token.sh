# optional: remove old origin if neccessary

git remote rm origin


# add user with access token to current repository

git remote add origin \
https://YOUR_GITHUB_USERNAME:$GITHUB_ACCESS_TOKEN@github.com/YOUR_GITHUB_USERNAME/REPO.git


# re-set origin 

git push --set-upstream origin main
