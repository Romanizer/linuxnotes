#!/bin/bash

# get video url and starttime
url=$1
starttime=$(date +%s.%N)

# cd into destination folder, where video is going to be saved
cd mp4

# Download video without videoID in filename
youtube-dl -o "%(title)s.%(ext)s" $url

# echo out download time
dur=$(echo "$(date +%s.%N) - $starttime" | bc)
printf "Download and Conversion completed in %.2f seconds\n" $dur
