#!/bin/bash
# get video url and start time
url=$1
starttime=$(date +%s.%N)

# cd into destination folder
cd mp3

# dl video with best audio and convert with ffmpeg to mp3
youtube-dl -f bestaudio -o "%(title)s" $url --exec "ffmpeg -hide_banner -v quiet -stats -i {}  -codec:a libmp3lame -qscale:a 0 {}.mp3 && rm {}" 

# eacho download duration
dur=$(echo "$(date +%s.%N) - $starttime" | bc)
printf "Download and Conversion completed in %.2f seconds\n" $dur
