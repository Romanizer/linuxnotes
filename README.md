# Linuxnotes

Useful Commands and scripts for Linux

## Overview

- automating daily tasks
- scripts to speed things up
- useful commands which often require re-googling to find

Most scripts are commented well and should run on most distros, unless specified otherwise.

## Scripts

### arch-clean-pkg-cache.sh

- removes all cached packages except latest version
- removes all cached uninstalled packages
- removes AUR and yay builds

## Markdown Reference

### Spoilers

<details>
    <summary>Spoiler summary text goes here.</summary>
    This is not to be spoiled.
</details>
