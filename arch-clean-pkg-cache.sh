#!/bin/bash

# remove all cached packages except most recent version

echo ""
echo "----- Removing all cached packages except latest one"
sudo paccache -rk1

# remove all cached but uninstalled packages

echo ""
echo "----- Removing all cached but uninstalled packages"
sudo paccache -ruk0

# remove cached packages/build from yay

echo ""
echo "----- Removing cached packages and builds from yay"
yay -Sc
