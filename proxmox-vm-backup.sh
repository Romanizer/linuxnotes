# backup a vm in proxmox to a remote host

# first find vm's id to back up
qm list

# back up vm while running with zstd compression and send to remote host
vzdump <vmid> --stdout --compress zstd | ssh user@remote 'cat > /path/to/backup.vma'

# might need to add user@remote's public key to .ssh/authorized_keys
# see ssh-keygen.sh
