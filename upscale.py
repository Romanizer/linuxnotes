import os
import sys

# get folder path argument
path = sys.argv[1];

for file in os.listdir(path):
    print(f"UPSCALING: {file}");
    file = os.path.join(path, file)
    # construct output file name
    file_out = file.split('.')[0]
    file_out += "_up.png"
    # upscale image
    os.system(f"waifu2x-ncnn-vulkan -n 2 -s 2 -i {file} -o {file_out}")
